#ifndef STATION_H
#define STATION_H

#include <stdio.h>
#include <string.h>
#include <string.h>
#include <dos.h>
#include <iostream>

void FillStation();
void ShowStation();

class Station
{
	public:
		
		long getStationNumber(void);
		void setStationNumber(long);

		std::string getStationName(void);
		void setStationName(std::string);
		

		std::string getTheRoute(void);
		void setTheRoute(std::string);

		long getStationPosition(void);
		void setStationPosition(long);
		
		Station();
		~Station();
	private:
		long station_number;
		std::string station_name;
		std::string the_route;
		long station_position;
};

#endif

		

	

