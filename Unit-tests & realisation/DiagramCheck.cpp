#include "Route.h"
#include "Station.h"
#include "Travel.h"
extern Travel T[40];
extern Station S[40];
extern Route R[10];

int checkTime(std::string);
int checkDate(std::string);
void Diagramm15(std::string,int,int,int);
void Diagramm18(std::string,int,int,int);

void CheckDiagram15(){
	std::string rout_use="Route_1";
	std::string date_1="01.10.2019";
	std::string date_2="02.10.2019";
	
	int ch_format1,ch_format2;
	ch_format1 = checkDate(date_1);
	ch_format2 = checkDate(date_2);
	std::cout<<"Date start ="<<ch_format1<<"| Date end = "<<ch_format2<<"\n";
	system("pause");		
	Diagramm15(rout_use,ch_format1,ch_format2,0);
	
	rout_use="Route_2";
	std::string time_1="15:00";
	std::string time_2="17:00";
	std::string date_d="02.10.2019";
	
	int ch_format3,ch_format4,ch_format5;
	ch_format3 = checkTime(time_1);
	ch_format4 = checkTime(time_2);
	ch_format5 = checkDate(date_d);
	std::cout<<"Time start ="<<ch_format3<<"| Time end = "<<ch_format4<<" | Date = "<<ch_format5<<"\n";
	Diagramm15(rout_use,ch_format3,ch_format4,ch_format5);
}

void CheckDiagram18(){
	
	std::string rout_use="Route_1";
	std::string date_1="01.10.2019";
	std::string date_2="02.10.2019";
	
	int ch_format1,ch_format2;
	ch_format1 = checkDate(date_1);
	ch_format2 = checkDate(date_2);
	std::cout<<"Date start ="<<ch_format1<<"| Date end = "<<ch_format2<<"\n";
	system("pause");		
	Diagramm18(rout_use,ch_format1,ch_format2,0);
	
	rout_use="Route_2";
	std::string time_1="15:00";
	std::string time_2="17:00";
	std::string date_d="02.10.2019";
	
	int ch_format3,ch_format4,ch_format5;
	ch_format3 = checkTime(time_1);
	ch_format4 = checkTime(time_2);
	ch_format5 = checkDate(date_d);
	std::cout<<"Time start ="<<ch_format3<<"| Time end = "<<ch_format4<<" | Date = "<<ch_format5<<"\n";
	Diagramm18(rout_use,ch_format3,ch_format4,ch_format5);
	
	
}
