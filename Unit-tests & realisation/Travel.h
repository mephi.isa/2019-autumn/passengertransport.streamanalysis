#ifndef TRAVEL_H
#define TRAVEL_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dos.h>
#include <iostream>

void FillTravel();
void ShowTravel();

class Travel
{
	public:
		void setTravelPosition(long);
		long getTravelPosition(void);
		void setStationMnemonic(std::string);
		std::string getStationMnemonic(void);
		void setAction(bool);
		bool getAction(void);
		void setCardNumber(std::string);
		std::string getCardNumber(void);
		void setDate(std::string);
		std::string getDate(void);
		void setTime(std::string);
		std::string getTime(void);
		
		Travel();
		~Travel();
	private:
		long travel_position;
		std::string station_mnemonic;
		bool action;
		std::string card_number;
		std::string date;
		std::string time;
};

#endif
