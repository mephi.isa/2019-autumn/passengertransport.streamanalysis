#include "Route.h"
#include "Station.h"
#include "Travel.h"

extern Route R[10];
extern Station S[40];
extern Travel T[40];
int checkDate(std::string);
int checkTime(std::string);
void CheckDiagram15();
void CheckDiagram16();
void CheckDiagram18();
void Diagramm15(std::string,int,int,int);//std::string trt,int date_time_st, int date_time_fin, int date_day)
void Diagramm18(std::string,int,int,int);

void CSVconvert(std::string, int*, int*, std::string,std::string);

int main (void){
	
	std::cout<<"Unit-testing by Liliia Gatilova.\n";
	
	//�������� ������� ������ ���������� � �������� ���� CSV.
	std::string c1="Time", c2="Entries";
	int t[2],e[2];
	t[0]=1020;
	t[1]=1040;
	e[0]=15;
	e[1]=65;
	
	CSVconvert("testcsv",t,e,c1,c2);
	
//����� �������� ����������������� ������� �������������� ���� � �������������� �������		
	
	std::string test_date;
	std::string test_time;
	int ch_format;
	do{	
		std::cout<<"\nEnter date \n";
		std::cin>>test_date;
		ch_format = checkDate(test_date);
		if(ch_format!=1) {
			std::cout<<"result of date conv="<<ch_format;	
		}else{
			std::cout<<"Bad date. Try again.";
		}
	}while(ch_format==1);
	
	do{
		std::cout<<"\nEnter time \n";
		std::cin>>test_time;
		ch_format = checkTime(test_time);
		if(ch_format!=1) {
			std::cout<<"result of time conv="<<ch_format;	
		}else{
			std::cout<<"Bad date. Try again.";
		}		
	}while(ch_format==1);
	
	

	//���������� ������ � �������� ������� ������������ ������ ���������� �� ���� ����� ��������
	//(��������� ��� ��������� ���������� �������� ���� � ���������� �������� �������� �������)
	std::cout<<"--------ROUTE-------\n";	
	FillRoute();
	ShowRoute();
	system("pause");
	std::cout<<"-------STATION-------\n";
	FillStation();
	ShowStation();
	system("pause");
	std::cout<<"--------TRAVEL-------\n";
	FillTravel();
	ShowTravel();
	system("pause");
	CheckDiagram15();
	CheckDiagram18();
	CheckDiagram16();
	
	return 0;
}


