#ifndef ROUTE_H
#define ROUTE_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dos.h>
#include <iostream>

void FillRoute();
void ShowRoute();

class Route
{
	public:
		Route();
		~Route();
	
		long getRouteNumber(void);
		void setRouteNumber(int);
		
		void setRouteName(std::string);
		std::string getRouteName(void);

	private:
 		long route_number;
 		std::string route_name;
};

#endif
