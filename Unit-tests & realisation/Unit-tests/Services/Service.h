#ifndef SERVICE_H
#define SERVICE_H
#include <string>
#include<BusinessObjects/DiagramData.h>
#include<QDate>

class Service
{
public:
    virtual std::string login(std::string login, std::string password)= 0;
    virtual void addUser(std::string uuid, std::string login, std::string password) = 0;
    virtual std::shared_ptr<DiagramData> getDiagramsData(std::string uuid, int type, std::string landOrRouting, int step, QDate start, QDate end) = 0;


    //virtual ~Service() = 0;
};
#endif // SERVICE_H
