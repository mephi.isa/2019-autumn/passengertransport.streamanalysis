#ifndef AUTHORIZATIONSERVICE_H
#define AUTHORIZATIONSERVICE_H

#include <string>
#include <Dao/Dao.h>

class AuthorizationService
{
public:
    AuthorizationService(std::shared_ptr<Dao> dao);
    bool checkCredentials(std::string login, std::string password);
    std::string login(std::string login, std::string password);
    void addUser(std::string uuid, std::string login, std::string password);
    void checkUuid(std::string uuid);
    void updateUuid(std::string uuid);
private:
    std::shared_ptr<Dao> dao;
};

#endif // AUTHORIZATIONSERVICE_H
