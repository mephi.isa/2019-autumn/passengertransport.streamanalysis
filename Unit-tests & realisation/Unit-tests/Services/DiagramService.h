#ifndef DIAGRAMSERVICE_H
#define DIAGRAMSERVICE_H
#include <Dao/Dao.h>
#include <BusinessObjects/Diagram.h>

class DiagramService
{
public:
    DiagramService(std::shared_ptr<Dao> dao);

    std::shared_ptr<DiagramData> getDiagramsData(int type, std::string landOrRouting, int step, QDate start, QDate end);

    std::string createMetadataForRequest(std::string landOrString, int step, QDate start, QDate end);
private:
    std::shared_ptr<Dao> dao;
    std::shared_ptr<Diagram> diagramList[6];
};

#endif // DIGRAMSERVICE_H
