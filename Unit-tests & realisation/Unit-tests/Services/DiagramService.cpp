#include "Services/DiagramService.h"

DiagramService::DiagramService(std::shared_ptr<Dao> dao)
{
    this->dao = dao;

    this->diagramList[0] = std::make_shared<Diagram>(Diagram(0));
    this->diagramList[0]->setType("Остановки", "Кол-во посадок");

    this->diagramList[1] = std::make_shared<Diagram>(Diagram(1));
    this->diagramList[1]->setType("Время", "Кол-во посадок");

    this->diagramList[2] = std::make_shared<Diagram>(Diagram(2));
    this->diagramList[2]->setType("Высадки", "Кол-во пассажиров");

    this->diagramList[3] = std::make_shared<Diagram>(Diagram(3));
    this->diagramList[3]->setType("Время", "Выручка");

    this->diagramList[4] = std::make_shared<Diagram>(Diagram(4));
    this->diagramList[4]->setType("Остановки", "Выручка");

    this->diagramList[5] = std::make_shared<Diagram>(Diagram(5));
    this->diagramList[5]->setType("Время", "Удаленность");

}

std::string DiagramService::createMetadataForRequest(std::string landOrRouting, int step, QDate start, QDate end){
    return landOrRouting + ":"
            + std::to_string(step) + ":"
            + start.toString().toUtf8().constData() + ":"
            + end.toString().toUtf8().constData();
}

std::shared_ptr<DiagramData> DiagramService::getDiagramsData(int type, std::string landOrRouting, int step, QDate start, QDate end){
    std::string metaData = createMetadataForRequest(landOrRouting, step, start, end);
    std::shared_ptr<Diagram> diagram = this->diagramList[type];
    DiagramData* diagramData = diagram->lookForStat(metaData, step);
    if (diagramData == nullptr){
        std::shared_ptr<DiagramData> diagramDataFromDao;
        switch (type) {
                case 0:
                    diagramDataFromDao = this->dao->getLandingScheduleByStopDiagramsData0(landOrRouting, step,  start, end);
                    break;
                case 1:
                    diagramDataFromDao = this->dao->getLandingScheduleByRevenueDiagramsData1(landOrRouting, step,  start, end);
                    break;
                case 2:
                    diagramDataFromDao = this->dao->getLandingScheduleByStationsDiagramsData2(landOrRouting, step,  start, end);
                    break;
                case 3:
                    diagramDataFromDao = this->dao->getScheduleByMediumDistanceDiagramsData3(landOrRouting, step,  start, end);
                    break;
                case 4:
                    diagramDataFromDao = this->dao->getRevenueScheduleByRoutingDiagramsData4(landOrRouting, step,  start, end);
                    break;
                case 5:
                    diagramDataFromDao = this->dao->getLandingByStopDiagramsData5(landOrRouting, step,  start, end);
                    break;
            }
        diagram->UpdateCache(diagram->getCurrStat(), step, diagramDataFromDao->getDataX(), diagramDataFromDao->getDataY(), metaData);
        diagramDataFromDao->setStatMetaData(metaData);
        return diagramDataFromDao;
    }
    auto dgrmData = std::make_shared<DiagramData>(DiagramData());
    dgrmData->setStep(diagramData->getStep());
    int count = 0;
    //костыли от Лили
    for (int i = 1; !diagramData->getDataX()[i].empty(); i++) {count = i;}
    //пытаюсь подружить сырые и смарт поинтеры
    dgrmData->setDataX(diagramData->getDataX(), count);
    dgrmData->setDataY(diagramData->getDataY(), count);
    dgrmData->setStatMetaData(metaData);
    dgrmData->setPrevStatPointer(diagramData->getPrevStatPointer());
    dgrmData->setInitStatMemPointer(diagramData->getInitStatMemPointer());

    return dgrmData;
}


