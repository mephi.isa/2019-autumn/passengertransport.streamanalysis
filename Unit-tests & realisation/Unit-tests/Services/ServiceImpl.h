#ifndef SERVICEIMPL_H
#define SERVICEIMPL_H

#include <Services/Service.h>
#include <Services/DiagramService.h>
#include <Services/AuthorizationService.h>

class ServiceImpl: public Service {
public:
    ServiceImpl(std::shared_ptr<Dao> dao);

    std::string login(std::string login, std::string password){
        return this->autorizationService.login(login, password);
    }

    void addUser(std::string uuid, std::string login, std::string password){
        this->autorizationService.addUser(uuid, login, password);
    }

    std::shared_ptr<DiagramData> getDiagramsData(std::string uuid, int type, std::string landOrRouting, int step, QDate start, QDate end){
        try {
            this->autorizationService.checkUuid(uuid);
            return this->diagramService.getDiagramsData(type, landOrRouting, step, start, end);
        } catch (const std::runtime_error& e) {
            throw e;
        }
    }

private:
    AuthorizationService autorizationService;
    DiagramService diagramService;
};

#endif // SERVICEIMPL_H
