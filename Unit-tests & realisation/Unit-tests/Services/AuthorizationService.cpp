#include "AuthorizationService.h"
#include <string>
#include <Dao/Dao.h>
#include <QUuid>

#define TIMEOUT 600000

AuthorizationService::AuthorizationService(std::shared_ptr<Dao> dao){
    this->dao = dao;
}

bool AuthorizationService::checkCredentials(std::string login, std::string password) {
    std::string sessionUuid;
    std::string psswd = dao->getUserPassword(login);

    if (psswd != password)
        return false;
    return true;
}

std::string AuthorizationService::login(std::string login, std::string password) {
    std::string sessionUuid;
    bool success = checkCredentials(login, password);
    if (success){
        sessionUuid = QUuid::createUuid().toString().toUtf8().constData();
        dao->addSession(login, sessionUuid);
        return sessionUuid;
    }
    throw std::runtime_error("password does not exist");
}

void AuthorizationService::addUser(std::string uuid, std::string login, std::string password) {
    checkUuid(uuid);
    if (this->dao->getRoleByLogin(this->dao->getLoginByUuid(uuid)) == ADMIN){
        bool success = checkCredentials(login, password);
        if (success)
            throw std::runtime_error("user alredy exist");
        this->dao->addUser(login, password);
    } else {
        throw std::runtime_error("user do not have admin rules");
    }
}

void AuthorizationService::checkUuid(std::string uuid){
    bool uuidIsExist = this->dao->checkSession(uuid);
    if (uuidIsExist){
        QDateTime sessionTime = this->dao->getSessionDate(uuid);
        if (sessionTime.msecsTo(QDateTime::currentDateTime()) <= TIMEOUT){
            this->dao->updateSession(uuid, QDateTime());
        } else {
            throw std::runtime_error("session is outdated");
        }
    } else {
        throw std::runtime_error("session does not exist");
    }

}

void AuthorizationService::updateUuid(std::string uuid){
   this->dao->updateSession(uuid, QDateTime::currentDateTime());
}

