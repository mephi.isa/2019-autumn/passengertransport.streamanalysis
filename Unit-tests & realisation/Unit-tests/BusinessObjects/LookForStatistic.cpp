#include "Diagram.h"

DiagramData* Diagram::lookForStat(std::string reqv_data, int stp) {
    /*сделать поиск от текущей статистики, и если это не относится к текущей статистике, то просматривать весь список
Указатели в статистиках на предыдущие статистики получать после загрузки в список объекта
сделать получение текущего указателя через метод, т.к. указатель приватный*/
    DiagramData* d;
    d = this->getCurrStat();
    long cch = 0;
    cch = this->getCacheStation();

    std::cout << "\n 1 Count of stat is:" << cch << "\n";
    if (this->getDiagramStatistics()->empty()) {
        std::cout << "\n 2 No statistics!\n";
        return NULL;
    } else {
        /*Проверка, а не запрашивают ли текущую статистику снова*/
        if (d->getStep() == stp && d->getStatMetaData() == reqv_data) {
            std::cout << "\n 3 It is current statistic!\n";
            return d;
        }
        /*Проверка, есть ли ссылка на предыдущую статистику
         * и совпадает ли запрос с метаданными текущей статистики, кроме шага*/
        else if (/*d->getPrevStatPointer() == NULL || */ d->getStatMetaData() != reqv_data) {
            std::cout << "\n 4 Searching through all statistics!\n";
            /*если не совпадает, просмотреть весь список*/
            return (this->searchThroughList(reqv_data, stp, cch));

        } /*предыдущая статистика есть и метаданные совпадают.*/
        else if (d->getPrevStatPointer() != NULL /* && d->getStatMetaData()==reqv_data*/) {
            /*Сохранение значения указателя на предыдущую от текущей
            * статистику и проверка на совпадение шага*/
            int stpp1, stpp2, stpp3;
            stpp1 = d->getStep();
            stpp2 = (d->getPrevStatPointer()->getStep());
            stpp3 = (d->getPrevStatPointer()->getPrevStatPointer()->getStep());
            std::cout << "\n 5 Search! " << stpp1 << stpp2 << stpp3 << "\n";
            int i = 0;
            do {
                d = (d->getPrevStatPointer());
                std::cout << "Step[" << i << "] = " << d->getStep() << "|" << stp << "\n";
                if (d->getStep() == stp) {
                    return d;
                    break;
                }
                i++;
                std::cout << "i=" << i << "\n";
            } while ((d->getPrevStatPointer()) != NULL);
            return NULL;
        } else {
            /*Поиск статистики по всему списку, если искомая статистика никак не связана с текущей*/
            std::cout << "\nNo positions related to current statistics!\n";
            return NULL;
        }
    }
}

DiagramData* Diagram::searchThroughList(std::string r_d, int stat_step, long cur_cache_st) {
    int q_of_stat = 0;
    for (auto stat_iter : (*(this->getDiagramStatistics()))) {
        std::cout << "\nLOOK FOR STAT!\n";
        std::cout << "Stat step:" << stat_iter.getStep() << "\n";
        std::cout << r_d << "\n";
        if (r_d.compare(stat_iter.getStatMetaData()) == 0 && stat_iter.getStep() == stat_step) {
            std::cout << "\nFound!\n";
            std::cout << stat_iter.getStatMetaData() << "\n";
            this->setCurrStat(&stat_iter);
            return this->getCurrStat();
            break;
        } else {
            q_of_stat++;
            if (q_of_stat == cur_cache_st) {
                std::cout << "No such statistic!\n";
                return NULL;
                break;
            }
            std::cout << "look next element...\n";
        }
    }
    return NULL;
}
