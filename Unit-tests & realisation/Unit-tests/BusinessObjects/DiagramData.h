#ifndef DIAGRAMDATA_H
#define DIAGRAMDATA_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <dos.h>
#include <iostream>

/*Gatilova Liliia. 11.11.2019 MEPhI*/

class DiagramData {
public:
    DiagramData();
    ~DiagramData();

    void setDataY(long*, int);
    void setDataX(std::string*, int);
    bool setStep(int st);
    void setStatMetaData(std::string m_d);
    void setInitStatMemPointer(DiagramData* dd_imem_p);
    void setPrevStatPointer(DiagramData* pr_st_p);

    long* getDataY(void);
    std::string* getDataX(void);
    int getStep(void);
    std::string getStatMetaData(void);
    DiagramData* getInitStatMemPointer();
    DiagramData* getPrevStatPointer(void);

private:
    std::string data_x[10];
    long data_y[10];
    int step;

    DiagramData* initStatMemPointer;
    DiagramData* prevStatPointer;
    std::string statMetaData;
};

#endif
