#include "Diagram.h"

void Diagram::setType(std::string x_n, std::string y_n) {
    setX(x_n);
    setY(y_n);
}
void Diagram::setCacheStation(long cchSt) {
    cache_station = cchSt;
}

void Diagram::setCurrStat(DiagramData* curr_d_stat) {
    if (curr_d_stat == NULL) {
        throw - 1;
    } else {
        curr_stat = curr_d_stat;
    }
}

std::string Diagram::getX() {
    return x;
}

std::string Diagram::getY() {
    return y;
}

int Diagram::getType() {
    return type;
}

long Diagram::getCacheStation() {
    return cache_station;
}

std::list<DiagramData>* Diagram::getDiagramStatistics(void) {
    return &diagramStatistic;
}

DiagramData* Diagram::getCurrStat(void) {
    return curr_stat;
}
