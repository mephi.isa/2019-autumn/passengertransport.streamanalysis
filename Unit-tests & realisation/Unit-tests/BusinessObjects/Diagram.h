#ifndef DIAGRAM_H
#define DIAGRAM_H

#include "DiagramData.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <dos.h>
#include <iostream>
#include <iterator>
#include <list>

/*Gatilova Liliia. 11.11.2019 MEPhI*/

class Diagram : public DiagramData {
public:
    Diagram(int type_of_d);
    ~Diagram();

    void setType(std::string x_n, std::string y_n);
    void setCacheStation(long);
    void setCurrStat(DiagramData* curr_d_stat);

    std::string getX();
    std::string getY();
    int getType();
    long getCacheStation();

    std::list<DiagramData>* getDiagramStatistics(void); //��������� ��������� �� ������ ���������
    DiagramData* getCurrStat(void); //��������� ��������� �� ������� ����������

    DiagramData* UpdateCache(DiagramData*, int step_size, std::string*, long*, std::string);
    DiagramData* lookForStat(std::string reqv_data, int stp);
    DiagramData* searchThroughList(std::string r_d, int stat_step, long cur_cache_st);

private:
    std::list<DiagramData> diagramStatistic;
    DiagramData* curr_stat = new DiagramData();

    void setX(std::string xx) {
        x = xx;
    }
    void setY(std::string yy) {
        y = yy;
    }

    std::string x;
    std::string y;
    int users_step;
    int type;
    long cache_station;
};

#endif
