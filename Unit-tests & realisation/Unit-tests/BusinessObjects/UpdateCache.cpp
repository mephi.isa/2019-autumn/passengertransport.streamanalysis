#include "Diagram.h"

DiagramData* Diagram::UpdateCache(DiagramData* ddata_obj, int step_size, std::string* dx, long* dy, std::string me_da) {
    /*метод добавления статистики в список статистик*/
    if (ddata_obj != NULL) {
        std::cout << "Previous statistic's [" << ddata_obj << "] step is: " << ddata_obj->getStep() << "\n";
    } else {
        std::cout << "No previous statistic!\n";
    }
    long quant;
    long cchst = 0;

    if (step_size <= 0) {
        std::cout << "\nStep size mast be positive integer. Cannot create statistic!\n";
        return NULL;
    } else if (dx[0].empty()) {
        std::cout << "\nNo statistic's data!\n";
        return NULL;
    } else {
        /*подсчет количества элементов входного массива*/
        for (int i = 0; !dx[i].empty(); i++) {
            if (dy[i] >= 0) {
                quant = i;
            } else {
                /*контрольный вывод извещения об ошибке*/
                std::cout << "\nNon-positive data in y-mass!!!\n";
                return NULL;
                break;
            }
        }
        quant++;
        /*заполнение полей статистики*/
        this->getCurrStat()->setDataY(dy, quant);
        this->getCurrStat()->setDataX(dx, quant);
        this->getCurrStat()->setPrevStatPointer(ddata_obj);
        this->getCurrStat()->setStatMetaData(me_da);
        this->getCurrStat()->setInitStatMemPointer(NULL);
        this->getCurrStat()->setStep(step_size);
        /*пересчет количества кэшированных статистик*/
        cchst = this->getCacheStation();
        cchst++;
        this->setCacheStation(cchst);
        cchst = this->getCacheStation();
        /*добавление статистики в список*/
        this->getDiagramStatistics()->push_back(*(this->getCurrStat()));
        /*Возврат указателя на статистику из списка, которая была сохранена,
         * для обратной связности статистик и интелелктуального поиска*/
        return &this->getDiagramStatistics()->back();
    }
}
