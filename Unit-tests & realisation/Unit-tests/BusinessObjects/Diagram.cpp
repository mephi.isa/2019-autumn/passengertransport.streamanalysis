#include "Diagram.h"

Diagram::Diagram(int type_of_d) {
    if (type_of_d < 0 || type_of_d > 5) {
        throw - 1;
    } else {
        type = type_of_d;
        cache_station = 0;
    }
}

Diagram::~Diagram() {
}
