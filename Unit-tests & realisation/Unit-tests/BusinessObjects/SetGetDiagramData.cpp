#include "DiagramData.h"

void DiagramData::setDataY(long* d_y, int q) {
    for (int j = 0; j < q; j++) {
        data_y[j] = d_y[j];
    }
}

void DiagramData::setDataX(std::string* d_x, int q) {
    for (int j = 0; j < q; j++) {
        data_x[j] = d_x[j];
    }
}

bool DiagramData::setStep(int st) {
    if (st > 0) {
        step = st;
        return true;
    } else {
        return false;
    }
}

long* DiagramData::getDataY(void) {
    return data_y;
}

std::string* DiagramData::getDataX(void) {
    return data_x;
}

int DiagramData::getStep(void) {
    return step;
}

void DiagramData::setStatMetaData(std::string m_d) {
    statMetaData = m_d;
}

void DiagramData::setInitStatMemPointer(DiagramData* dd_imem_p) {
    initStatMemPointer = dd_imem_p;
}

void DiagramData::setPrevStatPointer(DiagramData* pr_st_p) {
    prevStatPointer = pr_st_p;
}

std::string DiagramData::getStatMetaData(void) {
    return statMetaData;
}

DiagramData* DiagramData::getInitStatMemPointer(void) {
    return initStatMemPointer;
}

DiagramData* DiagramData::getPrevStatPointer(void) {
    return prevStatPointer;
}
