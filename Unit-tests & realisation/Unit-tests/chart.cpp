#include "chart.h"
#include "ui_chart.h"

chart::chart(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::chart)
{
    ui->setupUi(this);
}

chart::~chart()
{
    delete ui;
}


QChartView* chart::getChartView(std::shared_ptr<DiagramData> diagram){
    int count = 0;
    //костыли от Лили
    QBarSet *set = new QBarSet("data");
    QBarSeries *series = new QBarSeries();
    QStringList categories;

    long maxValue = 0;
    for (int i = 0; !diagram->getDataX()[i].empty(); i++) {
        count = i;
        categories << QString::fromStdString(diagram->getDataX()[i]);
        maxValue = diagram->getDataY()[i] > maxValue ? diagram->getDataY()[i] : maxValue;
        *set << diagram->getDataY()[i];
        series->append(set);
    }

   QChart *chart = new QChart();
   chart->addSeries(series);
   chart->setAnimationOptions(QChart::SeriesAnimations);


   //QStringList categories;

   QBarCategoryAxis *axisX = new QBarCategoryAxis();
   axisX->append(categories);
   chart->addAxis(axisX, Qt::AlignBottom);
   series->attachAxis(axisX);

   QValueAxis *axisY = new QValueAxis();
   axisY->setRange(0, maxValue * 1.1 );
   chart->addAxis(axisY, Qt::AlignLeft);
   series->attachAxis(axisY);

   chart->legend()->setVisible(true);
   chart->legend()->setAlignment(Qt::AlignBottom);

   QChartView *chartView = new QChartView(chart);
   chartView->setRenderHint(QPainter::Antialiasing);

   chartView->resize(820, 600);

   chartView->show();

   return chartView;
}
