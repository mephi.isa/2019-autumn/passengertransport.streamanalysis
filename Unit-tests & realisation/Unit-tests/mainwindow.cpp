#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "model.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_login_clicked()
{
    QString login = this->ui->lineEdit_login->text();
    QString password = this->ui->lineEdit_password->text();

    //Model model = Model(std::make_shared<DaoImpl>(DaoImpl()));
    extern Model model;

    if(model.login(login, password)){

        this->request = new RequestCreator(nullptr);
        request->show();
        hide();

//        this->chart = new class chart(this);

//        chart->getChartView();

        //this->setCentralWidget(chart->getChartView());
    }
}
