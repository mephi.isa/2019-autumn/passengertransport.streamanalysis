include(gtest_dependency.pri)

TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG += thread
CONFIG -= qt
QMAKE_CXXFLAGS += '-fprofile-arcs -ftest-coverage -lgcov'
QMAKE_LFLAGS += '-fprofile-arcs -ftest-coverage -lgcov'

win32-g++
    INCLUDEPATH += C:\QT\Libraries\QtcGtest-4.6.0-win-x64\include\

HEADERS +=     tst_businessobjectstratest.h \
    Classes/Diagram.h \
    Classes/DiagramData.h

SOURCES +=     main.cpp \
    diagramsgtesting.cpp \
    Classes/Diagram.cpp \
    Classes/DiagramData.cpp \
    Classes/SetGetDiagram.cpp \
    Classes/UpdateCache.cpp \
    Classes/SetGetDiagramData.cpp \
    Classes/LookForStatistic.cpp
