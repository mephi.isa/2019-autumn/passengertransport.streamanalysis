#include "gtest/gtest.h"
#include "Classes/Diagram.h"
#include "Classes/DiagramData.h"

TEST(TestDCaching, TypeGetter){
    /*тестирование метода получения типа созданной "диаграммы".
     * в конструкторе класса аргументом принимается номер статистики*/
    Diagram DtestObjC = Diagram(1);
    EXPECT_EQ(1,DtestObjC.getType());
}

TEST(TestDCaching, XGetter){
    /*тестирование метода получения обозначения оси X*/
    Diagram DtestObjX= Diagram(1);
    std::string filler1 ="Stations";
    std::string filler2 = "Revenue";
    DtestObjX.setType(filler1,filler2);
    EXPECT_EQ("Stations",DtestObjX.getX());
}

TEST(TestDCaching, YGetter){
    /*тестирование метода получения обозначения оси Y*/
    Diagram DtestObjY= Diagram(1);
    std::string filler1 ="Stations";
    std::string filler2 = "Revenue";
    DtestObjY.setType(filler1,filler2);
    EXPECT_EQ("Revenue",DtestObjY.getY());
}

TEST(TestDCaching, CchGetter){
    /*тестирование метода класса Diagram
     * по подсчету количества кэшированных статистик*/
    Diagram DtestObjC= Diagram(1);
    long CchSt=22;
    DtestObjC.setCacheStation(CchSt);
    EXPECT_EQ(22,DtestObjC.getCacheStation());
}


TEST(TestDCaching,CurrStatGetterTrue){
    DiagramData* Dd=new DiagramData();
    Diagram* D=new Diagram(0);
    D->setCurrStat(Dd);
    EXPECT_TRUE(D->getCurrStat());
}

TEST(TestDCaching,CurrStatGetterException){
    DiagramData* Dd=NULL;
    Diagram* D=new Diagram(0);
    EXPECT_ANY_THROW(D->setCurrStat(Dd));
}


TEST(TestLFMethod, LookForStatZeroList){
    /*тестирование метода поиска статистики в кэше
     * вариант с пустым списком статистик*/
    std::string users_reqv = "route210.05.1915.05.20";
    int cur_cch_count;
    DiagramData* Dd=new DiagramData();
    Diagram* Ddret=new Diagram(0);
    Ddret->setCacheStation(0);
    cur_cch_count=Ddret->getCacheStation();
    Ddret->searchThroughList(users_reqv,5,cur_cch_count);
    Dd = Ddret->lookForStat(users_reqv,2);
    EXPECT_FALSE(Dd);
}

TEST(TestLFMethod, LookForStatNoStat){
    /*тестирование метода поиска статистики в кэше
    вариант с отсутствием искомой статистики*/
    DiagramData* Dd=NULL;
    DiagramData* Ddret=new DiagramData();
    Diagram DtestObjU = Diagram(0);

    std::string xx[]={"station1b", "station2b","station3b"};
    std::string users_reqv = "route210.05.1915.05.20";
    long yy[]={12,678,245};
    int st=4;
    Ddret = DtestObjU.UpdateCache(NULL,st,xx,yy,users_reqv);
    std::cout<<"Returned step size:"<<Ddret->getStep()<<"\n";
    st=2;
    xx[0]="station1a";xx[1]= "station2a";xx[2]="station3a";
    yy[0]=4666;yy[1]=92772;yy[2]=91741;
    Ddret = DtestObjU.UpdateCache(Ddret,st,xx,yy,users_reqv);
    std::cout<<"Returned step size:"<<Ddret->getStep()<<"\n";
    Dd=Ddret;
    st=1;
    xx[0]="station1c";xx[1]= "station2c";xx[2]="station3c";
    yy[0]=789;yy[1]=984;yy[2]=46841;
    Ddret = DtestObjU.UpdateCache(Ddret,st,xx,yy,users_reqv);
    std::cout<<"Returned step size:"<<Ddret->getStep()<<"\n";
    std::cout<<"\nReturned:"<< Ddret <<"\n";
    users_reqv="route210.12.1822.12.18";
    Dd = DtestObjU.lookForStat(users_reqv,3);
    EXPECT_FALSE(Dd);
}

TEST(TestLFMethod, LookForStatTrueCurrStat){
    /*тестирование метода поиска статистики в кэше
    вариант с отсутствием искомой статистики*/
    DiagramData* Dd=NULL;
    DiagramData* Ddret=new DiagramData();
    Diagram DtestObjU = Diagram(0);

    std::string xx[]={"station1b", "station2b","station3b"};
    std::string users_reqv = "route210.05.1915.05.20";
    long yy[]={12,678,245};
    int st=4;
    Ddret = DtestObjU.UpdateCache(Dd,st,xx,yy,users_reqv);
    std::cout<<"Returned step size:"<<Ddret->getStep()<<"\n";
    Dd=Ddret;
    st=2;
    xx[0]="station1a";xx[1]= "station2a";xx[2]="station3a";
    yy[0]=4666;yy[1]=92772;yy[2]=91741;
    Ddret = DtestObjU.UpdateCache(Dd,st,xx,yy,users_reqv);
    std::cout<<"Returned step size:"<<Ddret->getStep()<<"\n";
    Dd=Ddret;
    st=1;
    xx[0]="station1c";xx[1]= "station2c";xx[2]="station3c";
    yy[0]=789;yy[1]=984;yy[2]=46841;
    Ddret = DtestObjU.UpdateCache(Dd,st,xx,yy,users_reqv);
    std::cout<<"Returned step size:"<<Ddret->getStep()<<"\n";
    std::cout<<"\nReturned:"<< Ddret <<"\n";
    users_reqv="route210.05.1915.05.20";
    Dd = DtestObjU.lookForStat(users_reqv,1);
    EXPECT_TRUE(Dd);
}

TEST(TestLFMethod, LookForStatTrueOneOfPrevStat){
    /*Вариант нахождения статситики в цепочке
     * от текущей статистики*/
    DiagramData* Dd=NULL;
    DiagramData* Ddret=new DiagramData();
    Diagram DtestObjU = Diagram(0);

    std::string xx[]={"station1b", "station2b","station3b"};
    std::string users_reqv = "route210.05.1915.05.20";
    long yy[]={12,678,245};
    int st=5;
    Ddret = DtestObjU.UpdateCache( NULL,st,xx,yy,users_reqv);
std::cout<<"Returned step size:"<<Ddret->getStep()<<"\n";
    st=3;
    xx[0]="station1a";xx[1]= "station2a";xx[2]="station3a";
    yy[0]=4666;yy[1]=92772;yy[2]=91741;
    Ddret = DtestObjU.UpdateCache( Ddret,st,xx,yy,users_reqv);
std::cout<<"Returned step size:"<<Ddret->getStep()<<"\n";
    st=2;
    xx[0]="station1c";xx[1]= "station2c";xx[2]="station3c";
    yy[0]=789;yy[1]=984;yy[2]=46841;
    Ddret = DtestObjU.UpdateCache( Ddret,st,xx,yy,users_reqv);
std::cout<<"Returned step size:"<<Ddret->getStep()<<"\n";
    std::cout<<"\nReturned:"<< Ddret <<"\n";
    users_reqv="route210.05.1915.05.20";
    Dd = DtestObjU.lookForStat(users_reqv,5);
    //std::cout<<"\nStep of current stat is:"<<Dd->getStep()<<"\n";
    EXPECT_TRUE(Dd);
}

TEST(TestLFMethod, LookForStatFalseNoneOfPrevStat){
    /*тестирование метода поиска статистики в кэше
     *вариант с отсутствием искомой статистики с  цепочке статистик*/
    DiagramData* Dd=NULL;
    DiagramData* Ddret=new DiagramData();
    Diagram DtestObjU = Diagram(0);
    std::string xx[]={"station1b", "station2b","station3b"};
    std::string users_reqv = "route210.05.1915.05.20";
    long yy[]={12,678,245};
    int st=5;
    Ddret = DtestObjU.UpdateCache( NULL,st,xx,yy,users_reqv);
std::cout<<"Returned step size:"<<Ddret->getStep()<<"\n";
    st=3;
    xx[0]="station1a";xx[1]= "station2a";xx[2]="station3a";
    yy[0]=4666;yy[1]=92772;yy[2]=91741;
    Ddret = DtestObjU.UpdateCache( Ddret,st,xx,yy,users_reqv);
std::cout<<"Returned step size:"<<Ddret->getStep()<<"\n";
    st=2;
    xx[0]="station1c";xx[1]= "station2c";xx[2]="station3c";
    yy[0]=789;yy[1]=984;yy[2]=46841;
    Ddret = DtestObjU.UpdateCache( Ddret,st,xx,yy,users_reqv);
std::cout<<"Returned step size:"<<Ddret->getStep()<<"\n";
    std::cout<<"\nReturned:"<< Ddret <<"\n";
    users_reqv="route210.05.1915.05.20";
    Dd = DtestObjU.lookForStat(users_reqv,1);
    EXPECT_FALSE(Dd);
}

TEST(TestLFMethod,LookForStatTrueOneFromList){
    /*тестирование метода поиска статистики в кэше.
     *вариант с присутствием искомой статистики, не связанной с текущей*/
    DiagramData* Dd=NULL;
    DiagramData* Ddret=new DiagramData();
    Diagram DtestObjU = Diagram(0);
    std::string xx[]={"station1b", "station2b","station3b"};
    std::string users_reqv = "route210.05.1915.05.20";
    long yy[]={12,678,245};
    int st=5;
    Ddret = DtestObjU.UpdateCache(NULL,st,xx,yy,users_reqv);
    std::cout<<"Returned step size:"<<Ddret->getStep()<<"\n";
//---------------------------------------------------------------
    st=2;
    xx[0]="station1e";xx[1]= "station2e";xx[2]="station3e";
    yy[0]=9463;yy[1]=115;yy[2]=7533;
    users_reqv = "route412.05.1920.05.20";
    DtestObjU.UpdateCache(NULL,st,xx,yy,users_reqv);
    //std::cout<<"Returned step size:"<<Ddret->getStep()<<"\n";
//---------------------------------------------------------------
    users_reqv = "route210.05.1915.05.20";
    st=3;
    xx[0]="station1a";xx[1]= "station2a";xx[2]="station3a";
    yy[0]=4666;yy[1]=92772;yy[2]=91741;
    Ddret = DtestObjU.UpdateCache( Ddret,st,xx,yy,users_reqv);
    std::cout<<"Returned step size:"<<Ddret->getStep()<<"\n";
    st=2;
    xx[0]="station1c";xx[1]= "station2c";xx[2]="station3c";
    yy[0]=789;yy[1]=984;yy[2]=46841;
    Ddret = DtestObjU.UpdateCache( Ddret,st,xx,yy,users_reqv);
    std::cout<<"Returned step size:"<<Ddret->getStep()<<"\n";

    std::cout<<"\nReturned:"<< Ddret <<"\n";
    users_reqv = "route412.05.1920.05.20";
    Dd = DtestObjU.lookForStat(users_reqv,2);
    EXPECT_TRUE(Dd);
}

TEST(TestLFMethod,LookForStatFalseNoPrevStat){
    /*тестирование метода поиска статистики в кэше
     *вариант с отсутствием цепочки статистик от текущей*/
    DiagramData* Dd=NULL;
    DiagramData* Ddret=new DiagramData();
    Diagram DtestObjU = Diagram(0);
    std::string xx[]={"station1b", "station2b","station3b"};
    std::string users_reqv = "route210.05.1915.05.20";
    long yy[]={12,678,245};
    int st=5;
    Ddret=DtestObjU.UpdateCache(NULL,st,xx,yy,users_reqv);
    std::cout<<"Returned step size:"<<Ddret->getStep()<<"\n";
//---------------------------------------------------------------
    st=2;
    xx[0]="station1e";xx[1]= "station2e";xx[2]="station3e";
    yy[0]=9463;yy[1]=115;yy[2]=7533;
    users_reqv = "route412.05.1920.05.20";
    Ddret=DtestObjU.UpdateCache(NULL,st,xx,yy,users_reqv);
    //std::cout<<"Returned step size:"<<Ddret->getStep()<<"\n";
//---------------------------------------------------------------
     std::cout<<"\nReturned:"<< Ddret <<"\n";
     Dd = DtestObjU.lookForStat(users_reqv,1);
     EXPECT_FALSE(Dd);
}

TEST(TestDdata,TestSetGetXmass){
    /*тестирование метода установки значений по оси X*/
    std::string xx[]={"date1", "date2","date3"};
    DiagramData dd;
    dd.setDataX(xx,3);
    EXPECT_TRUE(dd.getDataX());
}

TEST(TestDdata,TestSetGetYmass){
    /*тестирование метода установки значений по оси Y*/
    long yy[]={5000,880,6000};
    DiagramData dd;
    dd.setDataY(yy,3);
    EXPECT_TRUE(dd.getDataY());
}

TEST(TestDdata,TestSetGetISMPFalse){
    /*тестирование метода установки указателя
     * на первичную выборку.
     * Вариант с нулевым указателем*/
    DiagramData dd;
    dd.setInitStatMemPointer(NULL);
    EXPECT_FALSE(dd.getInitStatMemPointer());
}

TEST(TestDdata,TestSetGetISMPTrue){
    /*тестирование метода установки указателя
     * на первичную выборку.
     * Вариант с ненулевым указателем*/
    DiagramData dd,dmem;
    dd.setInitStatMemPointer(&dmem);
    EXPECT_TRUE(dd.getInitStatMemPointer());
}

TEST(TestDdata,TestSetGetPrevStPntTrue){
    /*тестирование метода установки указателя
     * на предыдущую статистику.
     * Вариант с ненулевым указателем*/
    DiagramData dd,dd_prev;
    dd.setPrevStatPointer(&dd_prev);
    EXPECT_TRUE(dd.getPrevStatPointer());
}

TEST(TestDdata,TestSetGetPrevStPntFalse){
    /*тестирование метода установки указателя
     * на предыдущую статистику.
     * Вариант с нулевым указателем*/
    DiagramData dd;
    dd.setPrevStatPointer(NULL);
    EXPECT_FALSE(dd.getPrevStatPointer());
}

TEST(TestDdata,TestSetGetStatMetD){
    /*тестирование метда установки
     * метаданных статистики*/
    DiagramData dd;
    std::string md = "route10:3016:0019.06.19";
    dd.setStatMetaData(md);
    EXPECT_EQ(md,dd.getStatMetaData());
}

TEST(TestDdata,TestSetGetStep){
    /*тестирование метода установки шага статистики*/
    DiagramData dd;
    int stp = 3;
    dd.setStep(stp);
    EXPECT_EQ(stp,dd.getStep());
}

TEST(TestDetails, TestConstrDiagram){
    /*тест проверяет выработку исключения
     * конструктором класса Diagram */
    EXPECT_ANY_THROW(Diagram(-5));
}

TEST(TestUpdMethod, TestUpdateNegativeStep){
    /*тестирование метода кэширование новой
     * статистики (добавдение в список.
     * вариант с неудавшимся добавлением*/
    std::string xx[]={"station1", "station2","station3"}, users_reqv = "route210.05.1915.05.19";
    long yy[]={12,678,245};

    DiagramData* Dd=new DiagramData();
    DiagramData* Ddret=new DiagramData();
    Diagram DtestObjU = Diagram(0);
    Ddret = DtestObjU.UpdateCache(Dd,-5,xx,yy,users_reqv);
    EXPECT_FALSE(Ddret);
}

TEST(TestUpdMethod, TestUpdateZeroStep){
    /*тестирование метода кэширование новой статистики
    вариант с неудавшимся добавлением.*/
    std::string xx[]={"station1", "station2","station3"}, users_reqv = "route210.05.1915.05.19";
    long yy[]={12,678,245};
    DiagramData* Dd=new DiagramData();
    DiagramData* Ddret=new DiagramData();
    Diagram DtestObjU = Diagram(0);
    Ddret = DtestObjU.UpdateCache(Dd,0,xx,yy,users_reqv);
    EXPECT_FALSE(Ddret);
}

TEST(TestUpdMethod, TestUpdateEmptyMass){
    /*тестирование метода кэширование новой статистики
     *вариант с пустым входным массивом*/
    std::string xx[3];
    std::string users_reqv = "route210.05.1915.05.19";
    long yy[]={12,678,245};

    DiagramData* Dd=new DiagramData();
    DiagramData* Ddret=new DiagramData();
    Diagram DtestObjU = Diagram(0);

    Ddret = DtestObjU.UpdateCache(Dd,1,xx,yy,users_reqv);
    EXPECT_FALSE(Ddret);
}

TEST(TestUpdMethod, TestUpdateBadYMass){
    /*тестирование метода кэширование новой статистики
     *вариант с отрицательными данными в входном массивом*/
    std::string xx[]={"station1", "station2","station3","station4"};
    std::string users_reqv = "route210.05.1915.05.19";
    long yy[]={12,678,0,-777};

    DiagramData* Dd=new DiagramData();
    DiagramData* Ddret=new DiagramData();
    Diagram DtestObjU = Diagram(0);

    Ddret = DtestObjU.UpdateCache(Dd,1,xx,yy,users_reqv);
    EXPECT_FALSE(Ddret);
}

TEST(TestUpdMethod, TestUpdateTrueWithPrevStat){
    /*тестирование метода кэширование новой статистики
     *вариант с удачным добавлением*/
    std::string xx[]={"station1", "station2","station3"};
    std::string users_reqv = "route210.05.1915.05.19";
    long yy[]={12,678,245};
    DiagramData* Dd=new DiagramData();
    DiagramData* Ddret=new DiagramData();
    Diagram DtestObjU = Diagram(0);
    Ddret = DtestObjU.UpdateCache(Dd,1,xx,yy,users_reqv);
    std::cout<<"\n Step = "<<Ddret->getStep()<<"\n";
    EXPECT_TRUE(Ddret);
}

TEST(TestUpdMethod, TestUpdateTrueFreshStat){
    /*тестирование метода кэширование новой статистики
     *вариант с удачным добавлением*/
    std::string xx[]={"station1", "station2","station3"};
    std::string users_reqv = "route210.05.1915.05.19";
    long yy[]={12,678,245};
    DiagramData* Ddret=new DiagramData();
    Diagram DtestObjU = Diagram(0);
    Ddret = DtestObjU.UpdateCache(NULL,1,xx,yy,users_reqv);
    std::cout<<"\n Step = "<<Ddret->getStep()<<"\n";
    EXPECT_TRUE(Ddret);
}

TEST(TestUpdMethod, TestUpdateTrueDoubleFreshStat){
    /*тестирование метода кэширование новой статистики (добавдение в список.
    вариант с удачным добавлением*/
    std::string xx[]={"station1", "station2","station3"};
    std::string users_reqv = "route210.05.1915.05.19";
    long yy[]={12,678,245};
    DiagramData* Ddret=new DiagramData();
    Diagram DtestObjU = Diagram(0);

    Ddret = DtestObjU.UpdateCache(NULL,1,xx,yy,users_reqv);
    std::cout<<"\n Step = "<<Ddret->getStep()<<"\n";

    xx[0]="station1";xx[1]= "station2";xx[2]="station3";
    users_reqv = "route415.05.1930.05.19";
    yy[0]=957;yy[1]=7725;yy[2]=981;
    Ddret = DtestObjU.UpdateCache(NULL,3,xx,yy,users_reqv);
    std::cout<<"\n Step = "<<Ddret->getStep()<<"\n";
    EXPECT_TRUE(Ddret);
}
