#include "Classes/BusinessObjects/Diagram.h"
#include "Classes/BusinessObjects/DiagramData.h"
#include "gtest/gtest.h"

#include "DaoImpl.h"
#include "AuthorizationService.h"
#include "DiagramService.h"

TEST(AuthTesting, successAuth) {
    auto daoImpl = std::make_shared<DaoImpl>(DaoImpl());
    AuthorizationService autorization = AuthorizationService(daoImpl);
    std::string uuid = autorization.login("login_analyst", "password_analyst");

    EXPECT_EQ(daoImpl->getSessionDate(uuid) , QDateTime::currentDateTime());
}

TEST(AuthTesting,failedAuth) {
    auto daoImpl = std::make_shared<DaoImpl>(DaoImpl());
    AuthorizationService autorization = AuthorizationService(daoImpl);
    EXPECT_ANY_THROW(autorization.login("blabla", "blabla"));
}

TEST(AuthTesting,checkUuidSuccess) {
    auto daoImpl = std::make_shared<DaoImpl>(DaoImpl());
    AuthorizationService autorization = AuthorizationService(daoImpl);
    EXPECT_NO_THROW(autorization.checkUuid("11111"));
}

TEST(AuthTesting,checkUuidOutdated) {
    auto daoImpl = std::make_shared<DaoImpl>(DaoImpl());
    AuthorizationService autorization = AuthorizationService(daoImpl);
    EXPECT_ANY_THROW(autorization.checkUuid("123456789"));
}

TEST(AuthTesting,checkUuidNotExist) {
    auto daoImpl = std::make_shared<DaoImpl>(DaoImpl());
    AuthorizationService autorization = AuthorizationService(daoImpl);
    EXPECT_ANY_THROW(autorization.checkUuid("1"));
}

TEST(AuthTesting,addUserSuccess) {
    auto daoImpl = std::make_shared<DaoImpl>(DaoImpl());
    AuthorizationService autorization = AuthorizationService(daoImpl);
    std::string adminUuid = autorization.login("login_admin", "password_admin");

    autorization.addUser(adminUuid, "new", "user");

    EXPECT_EQ(daoImpl->getUserPassword("new"), "user");
}

TEST(AuthTesting,addUserAlreadyExist) {
    auto daoImpl = std::make_shared<DaoImpl>(DaoImpl());
    AuthorizationService autorization = AuthorizationService(daoImpl);
    std::string adminUuid = autorization.login("login_admin", "password_admin");

    EXPECT_ANY_THROW(autorization.addUser(adminUuid, "login_analyst", "password_analyst"));
}

TEST(AuthTesting,addUserNoAdmin) {
    auto daoImpl = std::make_shared<DaoImpl>(DaoImpl());
    AuthorizationService autorization = AuthorizationService(daoImpl);
    std::string adminUuid = autorization.login("login_analyst", "password_analyst");

    EXPECT_ANY_THROW(autorization.addUser(adminUuid, "eeee", "bbbbb"));
}

TEST(AuthTesting, loginSuccess) {
    auto daoImpl = std::make_shared<DaoImpl>(DaoImpl());
    AuthorizationService autorization = AuthorizationService(daoImpl);
    std::string adminUuid = autorization.login("login_analyst", "password_analyst");

    EXPECT_NE(adminUuid, "");
}

TEST(AuthTesting, loginPasswordNotExist) {
    auto daoImpl = std::make_shared<DaoImpl>(DaoImpl());
    AuthorizationService autorization = AuthorizationService(daoImpl);
    EXPECT_ANY_THROW(autorization.login("aaaaa", "bbbb"));
}

TEST(DiagramTesting, create) {
    auto daoImpl = std::make_shared<DaoImpl>(DaoImpl());
    DiagramService diagramService = DiagramService(daoImpl);
    std::string req = diagramService.createMetadataForRequest("Москва",
                                                              2,
                                                              QDate::fromString("4-5-2015", "d-M-yyyy"),
                                                              QDate::fromString("10-5-2015", "d-M-yyyy"));
    EXPECT_EQ(req, "Москва:2:пн мая 4 2015:вс мая 10 2015");

}

TEST(DiagramTesting, getDiagramData) {
    auto daoImpl = std::make_shared<DaoImpl>(DaoImpl());
    DiagramService diagramService = DiagramService(daoImpl);
    std::shared_ptr<DiagramData> diagramData = diagramService.getDiagramsData(0,
                                                                              "land",
                                                                              1,
                                                                              QDate::currentDate(),
                                                                              QDate::currentDate());
    EXPECT_EQ(diagramData->getStatMetaData(), "land:1:ср дек. 25 2019:ср дек. 25 2019");
}

TEST(DiagramTesting, getDiagramDataFromCache) {
    auto daoImpl = std::make_shared<DaoImpl>(DaoImpl());
    DiagramService diagramService = DiagramService(daoImpl);
    std::shared_ptr<DiagramData> diagramData = diagramService.getDiagramsData(0,
                                                                              "land",
                                                                              1,
                                                                              QDate::currentDate(),
                                                                              QDate::currentDate());
    diagramData = diagramService.getDiagramsData(0,
                                                 "land",
                                                 1, QDate::currentDate(),
                                                    QDate::currentDate());
    EXPECT_EQ(diagramData->getStatMetaData(), "land:1:ср дек. 25 2019:ср дек. 25 2019");
}


