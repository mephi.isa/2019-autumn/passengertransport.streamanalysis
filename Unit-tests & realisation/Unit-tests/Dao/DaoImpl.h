#ifndef DAOIMPL_H
#define DAOIMPL_H

#include "Dao.h"
#include "QDateTime"
#include "Services/Role.h"

#include<map>
#include<stack>

class DaoImpl : public Dao
{
public:
    DaoImpl();

    struct User{
        std::string login;
        std::string password;
        Role role;
    };

    struct Session {
            User user;
            std::string uuid;
            QDateTime date;
    };


    Role getRoleByLogin(std::string login);

    std::shared_ptr<DiagramData> getLandingScheduleByStopDiagramsData0(std::string land, int step, QDate start, QDate end);
    std::shared_ptr<DiagramData> getLandingScheduleByStationsDiagramsData2(std::string land, int step, QDate start, QDate end);
    std::shared_ptr<DiagramData> getScheduleByMediumDistanceDiagramsData3(std::string land, int step, QDate start, QDate end);
    std::shared_ptr<DiagramData> getLandingByStopDiagramsData5(std::string land, int step, QDate start, QDate end);

    std::shared_ptr<DiagramData> getLandingScheduleByRevenueDiagramsData1(std::string routing, int step, QDate start, QDate end);
    std::shared_ptr<DiagramData> getRevenueScheduleByRoutingDiagramsData4(std::string routing, int step, QDate start, QDate end);

    void addSession(std::string login, std::string uuid);
    void updateSession(std::string uuid, QDateTime date);
    bool checkSession(std::string uuid);
    QDateTime getSessionDate(std::string uuid);

    std::string getLoginByUuid(std::string);

    void addUser(std::string login, std::string password);
    std::string getUserPassword(std::string login);

private:
    std::map <std::string, User> userMap;
    std::map <std::string, Session> sessionMap;
    std::stack <std::shared_ptr<DiagramData>> data[6];
};

#endif // DAOIMPL_H
