#include "DaoImpl.h"

DaoImpl::DaoImpl()
{

    struct User admin;
    admin.role = ADMIN;
    admin.login = "login_admin";
    admin.password = "password_admin";

    struct User analyst;
    analyst.role = ANALYST;
    analyst.login = "l";
    analyst.password = "p";


    struct User ex;
    ex.role = ANALYST;
    ex.login = "ex";
    ex.password = "ex";

    struct Session session;
    session.user = ex;
    session.uuid = "123456789";

    QDateTime valid = QDateTime::fromString("13.11.2012 19:52:07", "dd.MM.yyyy HH:mm:ss");
    session.date = valid;


    struct Session curSession;
    curSession.user = ex;
    curSession.uuid = "11111";
    curSession.date = QDateTime::currentDateTime();


    this->userMap.insert(std::pair<std::string, User>(admin.login, admin));
    this->userMap.insert(std::pair<std::string, User>(analyst.login, analyst));


    this->sessionMap.insert(std::pair<std::string, Session>("123456789", session));
    this->sessionMap.insert(std::pair<std::string, Session>("11111", curSession));


    auto diagamData00 = std::make_shared<DiagramData>(DiagramData());
    std::string xx00[] = {"stop1", "stop2", "stop3", "stop4"};
    diagamData00->setDataX(xx00, 4);
    long yy00[] = {100, 124, 115, 157};
    diagamData00->setDataY(yy00, 4);

    auto diagamData01 = std::make_shared<DiagramData>(DiagramData());
    std::string xx01[] = {"stop3", "stop5", "stop7", "stop9", "stop8"};
    diagamData01->setDataX(xx01, 5);
    long yy01[] = {123, 114, 157, 139, 124};
    diagamData01->setDataY(yy01, 5);

    auto diagamData10 = std::make_shared<DiagramData>(DiagramData());
    std::string xx10[] = {"step1", "step2", "step3", "step4", "step5"};
    diagamData10->setDataX(xx10, 5);
    long yy10[] = {321, 345, 367, 300, 378};
    diagamData10->setDataY(yy10, 5);

    auto diagamData20 = std::make_shared<DiagramData>(DiagramData());
    std::string xx20[] = {"land1", "land2", "land3", "land4", "land5"};
    diagamData20->setDataX(xx20, 5);
    long yy20[] = {34, 53, 29, 67, 37};
    diagamData20->setDataY(yy20, 5);

    auto diagamData30 = std::make_shared<DiagramData>(DiagramData());
    std::string xx30[] = {"11042014", "12042014", "13042014", "14042014", "15042014", "16042014", "17042014"};
    diagamData30->setDataX(xx30, 7);
    long yy30[] = {34, 53, 29, 67, 37, 31, 89};
    diagamData30->setDataY(yy30, 7);

    auto diagamData40 = std::make_shared<DiagramData>(DiagramData());
    std::string xx40[] = {"stop1", "stop2", "stop3", "stop4", "stop5", "stop6"};
    diagamData40->setDataX(xx40, 6);
    long yy40[] = {34000, 53000, 29000, 67000, 37000, 31000, 89000};
    diagamData40->setDataY(yy40, 6);

    auto diagamData50 = std::make_shared<DiagramData>(DiagramData());
    std::string xx50[] = {"step1", "step2", "step3", "step4", "step5"};
    diagamData50->setDataX(xx50, 5);
    long yy50[] = {321000, 345000, 367000, 300000, 378000};
    diagamData50->setDataY(yy50, 5);



    this->data[0].push(diagamData00);
    this->data[0].push(diagamData01);
    this->data[1].push(diagamData10);
    this->data[2].push(diagamData20);
    this->data[3].push(diagamData30);
    this->data[4].push(diagamData40);
    this->data[5].push(diagamData50);

}

Role DaoImpl::getRoleByLogin(std::string login){
    return this->userMap.find(login)->second.role;
}

void DaoImpl::addUser(std::string login, std::string password){
    struct User user;
    user.login = login;
    user.password = password;
    user.role = ANALYST;
    this->userMap.insert(std::pair<std::string, User>(user.login, user));
}

void DaoImpl::addSession(std::string login, std::string uuid){
    struct User user = this->userMap.find(login)->second;
    struct Session session;
    session.user = user;
    session.uuid = uuid;
    session.date = QDateTime::currentDateTime();
    this->sessionMap.insert(std::pair<std::string, Session>(session.uuid, session));
}

std::string DaoImpl::getUserPassword(std::string login){
    return this->userMap.find(login)->second.password;
}

QDateTime DaoImpl::getSessionDate(std::string uuid){
    return this->sessionMap.find(uuid)->second.date;
}

std::string DaoImpl::getLoginByUuid(std::string uuid){
    return this->sessionMap.find(uuid)->second.user.login;
}

bool DaoImpl::checkSession(std::string uuid){
    return this->sessionMap.count(uuid) > 0;
}

void DaoImpl::updateSession(std::string uuid, QDateTime date){
    this->sessionMap.find(uuid)->second.date = date;
}

std::shared_ptr<DiagramData> DaoImpl::getLandingScheduleByStopDiagramsData0(std::string land, int step, QDate start, QDate end){
    std::shared_ptr<DiagramData> data = this->data[0].top();
    //this->data[0].pop();
    return data;
}

std::shared_ptr<DiagramData> DaoImpl::getLandingScheduleByRevenueDiagramsData1(std::string land, int step, QDate start, QDate end){
    std::shared_ptr<DiagramData> data = this->data[1].top();
    //this->data[1].pop();
    return data;
}

std::shared_ptr<DiagramData> DaoImpl::getLandingScheduleByStationsDiagramsData2(std::string land, int step, QDate start, QDate end){
    std::shared_ptr<DiagramData> data = this->data[2].top();
    //this->data[2].pop();
    return data;
}

std::shared_ptr<DiagramData> DaoImpl::getScheduleByMediumDistanceDiagramsData3(std::string land, int step, QDate start, QDate end){
    std::shared_ptr<DiagramData> data = this->data[3].top();
    //this->data[3].pop();
    return data;
}

std::shared_ptr<DiagramData> DaoImpl::getRevenueScheduleByRoutingDiagramsData4(std::string land, int step, QDate start, QDate end){
    std::shared_ptr<DiagramData> data = this->data[4].top();
    //this->data[4].pop();
    return data;
}

std::shared_ptr<DiagramData> DaoImpl::getLandingByStopDiagramsData5(std::string land, int step, QDate start, QDate end){
    std::shared_ptr<DiagramData> data = this->data[5].top();
    //this->data[5].pop();
    return data;
}

