#ifndef DAO_H
#define DAO_H

//#include <unordered_map>
#include <string>
#include <Services/Role.h>
#include <list>
#include <BusinessObjects/Diagram.h>
#include <QDate>

class Dao
{
public:
    virtual Role getRoleByLogin(std::string login) = 0;

    virtual std::shared_ptr<DiagramData> getLandingScheduleByStopDiagramsData0(std::string land, int step, QDate start, QDate end) = 0;
    virtual std::shared_ptr<DiagramData> getLandingScheduleByStationsDiagramsData2(std::string land, int step, QDate start, QDate end) = 0;
    virtual std::shared_ptr<DiagramData> getScheduleByMediumDistanceDiagramsData3(std::string land, int step, QDate start, QDate end) = 0;
    virtual std::shared_ptr<DiagramData> getLandingByStopDiagramsData5(std::string land, int step, QDate start, QDate end) = 0;

    virtual std::shared_ptr<DiagramData> getLandingScheduleByRevenueDiagramsData1(std::string routing, int step, QDate start, QDate end) = 0;
    virtual std::shared_ptr<DiagramData> getRevenueScheduleByRoutingDiagramsData4(std::string routing, int step, QDate start, QDate end) = 0;

    virtual void addSession(std::string login, std::string uuid) = 0;
    virtual void updateSession(std::string uuid, QDateTime date) = 0;
    virtual bool checkSession(std::string uuid) = 0;
    virtual QDateTime getSessionDate(std::string uuid) = 0;
    virtual std::string getLoginByUuid(std::string) = 0;

    virtual void addUser(std::string login, std::string password) = 0;
    virtual std::string getUserPassword(std::string login) = 0;


};


#endif // DAO_H
