#include <stdio.h>
#include <string.h>
#include <iostream>
#include <stdlib.h>

int convertDate(std::string,std::string,std::string);
int convertTime(std::string,std::string);

int checkDate(std::string date_in){//������� �������� ������� ����, ��������� �������������
	std::string date_check,dd,mm,yyyy;
	int len;
	int dd1,mm1,y1;
	int res_date;
	
	date_check=date_in;	
	std::cout<<"date check="<<date_check<<"\n";
	
 if(date_check.length() != 10){
	std::cout<<"Bad length. Enter date in format dd.mm.yyyy\n";
	date_check.clear();
	return 1;
 }else{
		
	dd=date_check.substr(0,2);//������� ���������
	std::cout<<"dd="<<dd<<"\n";
	dd1=atoi(dd.c_str());//����������� ��������� � ��� int
	std::cout<<"dd1="<<dd1<<"\n";
	
	mm=date_check.substr(3,2);//������� ���������
	std::cout<<"mm="<<mm<<"\n";
	mm1=atoi(mm.c_str());//����������� ��������� � ��� int
	std::cout<<"mm1="<<mm1<<"\n";
	
	yyyy=date_check.substr(6,4);//������� ���������
	std::cout<<"yyyy="<<yyyy<<"\n";
	y1=atoi(yyyy.c_str());//����������� ��������� � ��� int
	std::cout<<"y1="<<y1<<"\n";

	if(y1<2019 || y1>2020){
		std::cout<<"Year mast be from 2019 to 2020\n";
		return 1;
	}else{		
		if(mm=="01" || mm=="03" || mm=="05" || mm=="07" || mm=="08" || mm=="10" || mm=="12"){
			if(dd1>31 || dd1<1){
				std::cout<<"Day mast be from 1 to 31\n";
				date_check.clear();
				return 1;	
			}else{
				res_date=convertDate(yyyy,mm,dd);
				std::cout<<res_date<<"\n";
				return res_date;
			}
		}else if (mm=="04" || mm=="06" || mm=="09" || mm=="11"){
			if(dd1>30 || dd1<1){
				std::cout<<"Day mast be from 1 to 30\n";
				date_check.clear();
				return 1;	
			}else{
				res_date=convertDate(yyyy,mm,dd);
				std::cout<<res_date<<"\n";
				return res_date;
			}
		}else if(mm=="02"){ 
			if(dd1>28 || dd1<1){
				std::cout<<"Day mast be from 1 to 28\n";
				date_check.clear();
				return 1;	
			}else{
				res_date=convertDate(yyyy,mm,dd);
				std::cout<<res_date<<"\n";
				return res_date;
			}			
		}else{
		std::cout<<"Enter date in format dd.mm.yyyy\n";
		std::cout<<"Month mast be from 1 to 12\n";
		date_check.clear();
		return 1;
		}
	}		
	
 }
	
	
		
	

}


int checkTime(std::string time_in){
	std::string time_check=time_in;
	std::cout<<"time check="<<time_check<<"\n";
	
	std::string hh,min;
	int hh1, min1,res_time;
	
 if(time_check.length() != 5){
	std::cout<<"Bad length. Enter time in format hh:mm\n";
	time_check.clear();
	return 1;
	}
 else{
 	hh=time_check.substr(0,2);//������� ���������
	std::cout<<"hh="<<hh<<"\n";
	hh1=atoi(hh.c_str());//����������� ��������� � ��� int
	std::cout<<"hh1="<<hh1<<"\n";
	
	min=time_check.substr(3,2);//������� ���������
	std::cout<<"min="<<min<<"\n";
	min1=atoi(min.c_str());//����������� ��������� � ��� int
	std::cout<<"min1="<<min1<<"\n";
	
	if(hh1<0 || hh1>23){
		std::cout<<"Bad hours. Hours mast be from 00 to 23\n";
		time_check.clear();
		return 1;
	}else{
		if(min1<0 || min1>59){
			std::cout<<"Bad minutes. Minuts mast be from 00 to 59\n";
			time_check.clear();
			return 1;
		}else{
			res_time=convertTime(hh,min);
			std::cout<<"Result time="<<res_time<<"\n";
			return res_time;
		}
	}
	
 }
	
}




int convertDate(std::string y,std::string m,std::string d){
		std::string conv_date=y+m+d;
		int new_date=atoi(conv_date.c_str());
		return new_date;
}

int convertTime(std::string h,std::string mn){
		std::string conv_time=h+mn;
		int new_time=atoi(conv_time.c_str());
		return new_time;
}
