#include "Route.h"
#include "Station.h"
#include "Travel.h"
extern Travel T[40];
extern Station S[40];
extern Route R[10];

int checkTime(std::string);
int checkDate(std::string);
void Diagram16(std::string,int, int, int);

void CheckDiagram16(){

	std::string station_use="Altufievo";
	std::string date_1="01.10.2019";
	std::string date_2="02.10.2019";
	
	int ch_format1,ch_format2;
	ch_format1 = checkDate(date_1);
	ch_format2 = checkDate(date_2);
	std::cout<<"Date start ="<<ch_format1<<"| Date end = "<<ch_format2<<"\n";
	system("pause");		
	Diagram16(station_use,ch_format1,ch_format2,0);
	
	
	station_use="Altufievo";
	std::string time_1="06:00";
	std::string time_2="07:00";
	std::string date_d="01.10.2019";
	
	int ch_format3,ch_format4,ch_format5;
	ch_format3 = checkTime(time_1);
	ch_format4 = checkTime(time_2);
	ch_format5 = checkDate(date_d);
	std::cout<<"Time start ="<<ch_format3<<"| Time end = "<<ch_format4<<" | Date = "<<ch_format5<<"\n";
	Diagram16(station_use,ch_format3,ch_format4,ch_format5);
	
}
