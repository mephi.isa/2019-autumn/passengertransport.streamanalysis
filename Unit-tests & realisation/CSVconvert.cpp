#include "Route.h"
#include "Station.h"
#include "Travel.h"
#include <fstream>
#include <ostream>
#include <istream>

void CSVconvert(std::string file_name, int* mass_1, int* mass_2, std::string col_1,std::string col_2){
	
	file_name=file_name+".csv";
	int size_of_name = file_name.size()+1;
	std::cout<<"Size of filename = "<<size_of_name<<"\n";
	system("pause");
	char * f_name= new char[size_of_name];
	strcpy(f_name, file_name.c_str());
	
	f_name[size_of_name]='\0';
	for(int j=0;j<size_of_name-1;j++){
		std::cout<<f_name[j];
	}
	
	std::ofstream fileCSV;
	fileCSV.open(f_name);
	int n=sizeof(mass_1)/sizeof(int);
	std::cout<<"\nMass size="<<n<<"\n";
	system("pause");
	fileCSV<<col_1<<";"<<col_2<<"\n";
	system("pause");
	for(int i=0;i<n;i++){
		fileCSV<<mass_1[i]<<";"<<mass_2[i]<<",\n";
	}
	fileCSV.close();
	
}
